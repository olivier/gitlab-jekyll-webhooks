var express = require('express');
var http = require('http');
var webhook = require('gitlab-webhook');
var app = express();

app.use(express.bodyParser());
app.use(express.methodOverride());
app.gitlab('/doc', {
  exec: 'sh ./build-static.sh {{repository.name}} {{repository.url}}',
  token: 'cool',
  branches: 'refs/heads/doc'
});
// serve static websites
app.use('/doc', express.directory(__dirname+'/static'));
http.createServer(app).listen(3000);

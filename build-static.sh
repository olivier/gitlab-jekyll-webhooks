#!/usr/bin/env bash

# destination directory to build doc in.
DEST="./repositories"
# where to generate static websites
BUILDS="./static"
# branch to build
BRANCH=doc

REPOSITORY=$1
CLONE_URL=$2

CLONE_PATH="${DEST}/${REPOSITORY}/"
BUILD_PATH="${BUILDS}/${REPOSITORY}/"


echo Will build $REPOSITORY in $CLONE_PATH

# ensure destination is created
if [ ! -e $DEST ]; then
    mkdir -p $DEST
fi

# clone or update
if [ -e $CLONE_PATH ]; then
    rm -rf $CLONE_PATH
fi
git clone --branch=$BRANCH --depth=50 $CLONE_URL $CLONE_PATH

echo "generate static site in $BUILD_PATH"
# remove old version
if [ -e $BUILD_PATH ]; then
    rm -rf $BUILD_PATH
fi
# ensure directory is present
if [ ! -e $BUILD_PATH ]; then
    mkdir -p $BUILD_PATH
fi
echo "bundle exec jekyll build --source $CLONE_PATH --destination $BUILD_PATH --trace"
bundle exec jekyll build --source $CLONE_PATH --destination $BUILD_PATH --trace
ls $BUILD_PATH
echo OK
